# Caracterización del muestreo de un área mediante simulaciones

  En este repositorio se encuentran varias herramientas que comenzamos a desarrollar con el objeto de simular diversas situaciones experimentales y estimar la capacidad de varias estrategias de muestreo para capturar información en un área.
  Modelar un muestreo ayuda a estimar cuántas muestras son necesarias para conocer una situación con suficiente certeza antes de realizar la experiencia, estimar la representatividad de los resultados y optimizar al máximo el uso de recursos.
  El modelo es general pero intenta responder a las preguntas sobre varios casos reales donde imaginamos que puede aplicarse.

## Cuerpo Principal del Informe

* Disponible en [este enlace](https://octavioduarte.gitlab.io/bootstrap_area/informe).


## Algunas situaciones de aplicación concretas

### Detección de fusarium en tierras destinadas al tomate.

### Detección de castores asilvestrados.

### Detección de contaminantes en un curso de agua.


## Detalles sobre los modelos estudiados


### Modelo Simple

  En este caso modelamos una región contaminada como un área donde las trazas de cada contaminante distribuyen normalmente desde un centro donde ocurrió la conaminación puntual.
  La densidad en el área representa la plausibilidad de detección a medida que el contaminante se disuelve, de tal formma que hay menos probabilidad de detección si se está más lejos de la fuente de contaminación.


#### Estado de este proyecto

  Desarrollamos la simulación como una función fácilmente iterable y mostramos los resultados para  un juego concreto de parámetros.
  Se simuló con éxito y eficientemente un caso trivial.

#### Siguientes pasos

  Ya está listo para una simulación más realista.

1. Elegir un juego óptimo de parámetros de interés del instrumento, la contaminación y una distribución realista de la contaminación.
2. Simular diversos tamaños de muestra y situaciones reales para caracterizar un proyecto real.


### Modelo multietápico


  La heurística original de este modelo es recuperar un gradiente en base al muestreo sobre un área. Es decir: conocer concretamente cómo está distribuída la contaminación a través de un muestreo en dos etapas.
  El primer muestreo es un muestreo de detección, sobre una *grilla gruesa*. Para cada punto de esta grilla donde se detecte contaminación, se define una *grilla fina* en sus inmediaciones. Sobre estas grillas finas se muestrea en mayor detalle.
  Con los nuevos valores obtenidos, se elabora un mapa de la densidad de la distribución de contaminantes usando un método no paramétrico de estimación por núcleos.


#### Estado del proyecto

  Se terminó una simulación piloto. Falta definir una medida de efectividad y matematizarla. La propuesta es usar cúmulos y tomar el centroide de cada cúmulo para después medir la distancia de cada centroide al centro de contaminación más cercano.


#### Siguientes pasos

1. Terminar de definir una métrica de efectividad.
2. Implementar una función iterable para poder simular un caso trivial.
3. Informar un caso no trivial más sofisticado para caracterizar un proyecto real.

#### Implicaciones posibles

  Como todo modelo realimentado de información, este modelo tiene aplicaciones a la teoría de control. Podría servir para desarrollar un robot muestreador que "olfatee" a través del gradiente usando los test disponibles hasta detectar un objetivo o generar un mapa de una precisión razonable.

#### Imagen de un gradiente recuperado por el algoritmo

![Gradiente Recuperado](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/muestreo/simulación_paramétrica/densidad_recuperada.png)