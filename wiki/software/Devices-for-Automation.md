# Software adapters for devices and automation 

Participantes: Nicolás Méndez, Fran Quero, Wladimir Araya.

Este tema es en parte una continuación del post en el foro de GOSH "Interfaces for communicating hardware devices": https://forum.openhardware.science/t/interfaces-for-communicating-hardware-devices/4287

En la residencia hablamos sobre cómo "adaptadores" modulares, que permitan conectar dispositivos de laboratorio a una PC e interactuar con ellos de forma remota.

Identificamos Python como el lenguaje más sensato para escribir estos adaptadores, y los siguientes protocolos de comunicación como más prioritarios:

- Serial (e.g. Arduino via USB).
- HTTP (e.g. ESP32 via redes IP).

Se propone una estructura similar al JSON-RPC para intercambiar información. Bajo este esquema, los mensajes se mandan en formato JSON y contienen:

1. Un identificador único para el mensaje (el "ID").
2. El nombre de la función que hay que ejecutar en el dispositivo (o en la PC).
3. Los datos que la función debe aceptar (de ser necesario).

Implementaciones:

- Del lado del host, el resultado es el módulo "coorder" del proyecto Pipettin: https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master/code/extra/coorder
    - El módulo usa `asyncio`.
- Del lado de Arduino, usamos ArduJSON para leer y mandar mensajes de JSON via serial, en el PocketPCR: https://gitlab.com/pipettin-bot/forks/labware/PocketPCR
