# Microscopio OpenFlexure

![OFM](/imagenes/hardware/microscopía/ofm/ofm-pao3.jpg) 


Este [OFM](https://openflexure.org/projects/microscope/build) es la versión 7 (alfa) de alta resolución con objetivo de 40X con cámara RPi4, lente doublet acromático


Paola Larrauli estuvo poniendo a punto su OFM para hacer pruebitas con el [chip de microfluidica](https://gitlab.com/luherbert/wiki-regosh-2023/-/blob/main/wiki/hardware/microscop%C3%ADa/microfluidica.md?ref_type=heads)

