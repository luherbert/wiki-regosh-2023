# Armado de Microscopio con ESP32

## Preliminares

* Este es el [repositorio](https://github.com/Matchboxscope/Matchboxscope/blob/master/Matchboxscope.md)

* Usamos una [ESP32-cam](https://tienda.ityt.com.ar/plataforma-esp32/11273-kit-esp32-cam-esp32-cam-mb-m%C3%B3dulo-camara-usb-wifi-itytarg.html)

![Placa, escudo y cable](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/esp32_cam.jpeg)

* [Repositorio del saleroscopio](https://gitlab.com/FernanFederici/saleroscopio/-/tree/master)
 ![Placa, escudo y cable](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/saleroscopio1.jpeg)

## Carga del Firmware

### Síntesis

  El procedimiento está bien documentado en [la página de documentación](https://github.com/Matchboxscope/matchboxscope-fw).
  En el repositorio explican cómo usar la IDE de Arduino para quemarla en el dispositivo, que es la vía  más usual. Nos encontramos con la dificultad de que añadir las modificaciones que hacen que esta IDE trabaje con el ESP32 no es trivial por cuestiones de versionado de la IDE. En algunas, la opción está oculta, en otras no funciona.
  Los autores también proporcionan un sistema para quemar usando un navegador compatible con Chrome (*Chrome*, *Chromium* o *Edge* fundamentalmente). Este sistema resultó ser sencillo y efectivo.


### Cómo usar el sistema en línea para quemar la lógica en el controlador

* Está alojado [en este enlace](https://matchboxscope.github.io/firmware/FLASH.html).

#### Pasos

1. Ensamblar la cámara y el ESP32 para que tengan alimentación de alguna forma y añadir un programador USB. Existe un escudo (shield) que proporciona ambas cosas y encastra sobre los pines.
2. Conectar la cámara al puerto USB. Asegurarse de que se inicie un simulador de puerto serie. Ver la NOTA SOBRE CONTROLADORES. Una alternativa sencilla es intentar y ver si funciona al primer intento. En caso de fallo, ver nota.
3. En la sección **Matchboxscope/Anglerfish (ESP32** Flashing Tool
** hay imágenes de varias herramientas del proyecto. Seleccionar el microscopio. Seleccionar la que tiene la imagen del *matchboxscope*, debería mostrar abajo el título **ESP32 Camera Simple Webcam Server Advanced**.
4. Si el navegador es compatible, un botón azul justo bajo las imágenes de los instrumentos, con el texto **CONNECT**.
5. La aplicación da mensajes de estado muy claros hasta que termina. Toma como mucho 5 minutos.


#### NOTA SOBRE CONTROLADORES

  Para poder programar el controlador, es necesario tener un puerto serie. Esto en general se hace con un adaptador de USB a puerto serie (UART). Esto requiere controladores específicos. En Linux, el controlador más común (`ch30x`) es parte de la mayoría de las versiones del núcleo que se distribuyen. En otros sistemas, hay que descargar e instalar el controlador, por ejemplo desde el sitio de Arduino.
  Si se tienen dudas sobre la presencia del controlador en Linux, se puede usar el comando `dmesg`, que muestra los registros de actividad del núcleo. Enchufar el usb a la computadora y al controlador y ejecutar el comando.
  Las últimas líneas deberían confirmar que se inició la emulación de un puerto serial por USB.
  Detectamos un **problema común** en Ubuntu, donde `apparmor` se opone a permitir el uso de este puerto a Chrome. En este caso, habilitar ese puerto en `apparmor`.


#### Ejemplo del texto de una conexión exitosa en `dmesg`

```
[61565.313946] usb 1-2: new full-speed USB device number 3 using xhci_hcd
[61565.454199] usb 1-2: New USB device found, idVendor=1a86, idProduct=7523, bcdDevice= 2.54
[61565.454210] usb 1-2: New USB device strings: Mfr=0, Product=2, SerialNumber=0
[61565.454214] usb 1-2: Product: USB2.0-Ser!
[61565.505471] usbcore: registered new interface driver usbserial_generic
[61565.505483] usbserial: USB Serial support registered for generic
[61565.507524] usbcore: registered new interface driver ch341
[61565.507538] usbserial: USB Serial support registered for ch341-uart
[61565.507564] ch341 1-2:1.0: ch341-uart converter detected
[61565.519256] ch341-uart ttyUSB0: break control not supported, using simulated break
[61565.519361] usb 1-2: ch341-uart converter now attached to ttyUSB0
```

## Opciones para la cámara

Para poder utilizar la cámara, hay dos opciones:

1) Cámara con entrada USB incluída

![Opción 1](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/camara_usb1.jpg)

![Opción 1](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/camara_usb2.jpg)


2) Cámara más shield

Shield necesario para cargar el firmware y alimentarla
Una vez cargado el firmware, se puede prescindir del shield y alimentar la cámara directamente

![Opción 2](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/camara_shield1.jpg)

![Opción 2](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/camara_shield2.jpg)

![Opción 2](https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/esp32-cam/camara_shield3.jpg)

