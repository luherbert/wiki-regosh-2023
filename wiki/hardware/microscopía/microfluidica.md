# Chip microfluídica

Estuvimos jugando por primera vez con el chip de microfluidica tipo escalera [chip de microfluídica escalera](https://chemrxiv.org/engage/chemrxiv/article-details/63fb0342937392db3d1126e6) con la idea de usarlo en muestras de agua o suelo para separar y ver microorganismos.

<figure align="center">
    <img src="https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/microfluidica/microfluidica1.jpeg" alt="Microfluídica" width="300"/> 
    <img src="https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/microscopía/microfluidica/microfluidica2.jpeg"  alt="Microfluídica" width="300"/>
    <figcaption style="font-size:small"> Imagenes cortesía de Vittorio Saggiomo  </figcaption>
</figure>

Nuestro set up. Solo teníamos una jeringa que usamos para hacer pasar la muestra. Esta operación exige bastante contrapresión por lo que en principio parece dificil correr mucha muestra.

<figure align="center">
    <img src="/imagenes/hardware/microscop%C3%ADa/microfluidica/microfluidica3.jpeg" alt="Setup de microfluídica" width="500"/> 
</figure>


Para jugar tomamos una muestra de agua y luego otra de suelo.  

## Prueba 1
Primero probamos con el  con un OFM 40X que Paola Larrauli (@pla_ltl) puso a punto pero no logramos hacer foco porque el vidrio donde está pegado el PDMS es muy grueso: hay que aumentar la distancia focal o utilizar un vidrio de 0.17mm.

> Discutimos si es posible reciclar los vidrios de las placas de microscopia grandes, que son de 0.17 mm.
> El adhesivo que usan para pegar el vidrio (gernam glass?) a la placa es "USP class 4", quizás sea una silicona, y haya que sacarlo con algún solvente power y calor. 

## Prueba 2
Cambiamos el objetivo 40X por un 25X y pudimos hacer foco. Vimos que para utilizar el microchip con muestras de suelo conviene diluir un poco mas.   

<figure align="center">
    <img src="/imagenes/hardware/microscop%C3%ADa/microfluidica/ofm-pao2.jpg" alt="OFM con 25X" width="500"/> 
</figure>
