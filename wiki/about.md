# ¿Cómo documentar?

## Sistema a grandes rasgos

### Directorios y cuerpos de texto

Cada canal del discord es un subdirectorio de la carpeta Wiki.
Cada hilo adentro del canal es un subidredirectorio de la carpeta del canal.
La Wiki levanta esta estructura automáticamente y organiza los archivos en el índice.
Cada contenido se desarrolla en un archivo Markdown. Lo ideal es tener uno por tópico dentro de su directorio.
Mirar el caso ya existente.

### Imágenes

Todas las imágenes están en el discord o en el [drive de la residencia](https://drive.google.com/drive/folders/1ywrqJ-rQ0oDKdN6jntYbiOlSXBvtbsxx).
Las imágenes de cada documento van en la carpeta `imágenes` con la misma jerarquía que los cuerpos de texto y cada imagen tiene un nombre compuesto que tiene el mismo nombre inicial que el cuerpo de texto donde se va a enlazar, más una diferenciación significativa.
Para usarlas en la Wiki, se necesita copiarlas a la carpet imágenes y enlazarlas en el repo. El enlace base es `https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/`, seguido del directorio. 

Así queda una de las imágenes del artículo ejemplo: `https://gitlab.com/luherbert/wiki-regosh-2023/-/raw/main/imagenes/hardware/esp32-cam/esp32_cam.jpeg`.

### Caso existente: Microscopio ESP32 CAM

La documentación original está en Discord en [Hardware/#esp32cam](https://discordapp.com/channels/1146170276275560459/1149774383607463946/1149786576298180638).


Dentro de Wiki, creamos la carpeta Hardware y el archivo Markdown `microscopio esp32-cam` adentro de ella.

* [Microscopio ESP32 CAM Wiki](https://luherbert.gitlab.io/wiki-regosh-2023/Hardware/esp32-cam/)
* [Microscopio ESP32 CAM Directorio texto](https://gitlab.com/luherbert/wiki-regosh-2023/-/tree/main/wiki/Hardware?ref_type=heads)
* [Microscopio ESP32 CAM Directorio imágenes](https://gitlab.com/luherbert/wiki-regosh-2023/-/tree/main/imagenes/hardware/esp32-cam?ref_type=heads)
